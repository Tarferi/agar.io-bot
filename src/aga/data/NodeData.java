package aga.data;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.geom.Rectangle2D;

import agar.GUI.mainGUI;

public class NodeData {

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getSize() {
		return size;
	}

	public final int id;
	private int y;
	private int x;
	private static int xMin, yMin, xMax, yMax;
	private int size;
	public final Color color;
	public String name;
	public final boolean virus;
	private int sizeHalf = 1;
	private static final Font defaultFont = new Font("Arial", Font.PLAIN, 12);
	private boolean dead = false;
	private static final Image virusImg = ImageSource.Virus;

	public static final void paintBorder(Graphics g, int offX, int offY) {
		g.setColor(Color.black);
		g.drawRect(xMin, yMin, xMax - xMin, yMax - yMin);
	}

	public static final void setBorder(int x1, int x2, int y1, int y2) {
		xMin = x1;
		xMax = x2;
		yMin = y1;
		yMax = y2;
	}

	public NodeData(mainGUI gui, int id, int posX, int posY, int sizer, short rc, short gc, short bc, String name, boolean virus) {
		this.id = id;
		this.x = posX;
		this.y = posY;
		this.size = sizer + sizer;
		this.sizeHalf = sizer;
		this.color = new Color(rc, gc, bc);
		this.name = name;
		this.virus = virus;
	}

	public void paint(Graphics g, int offX, int offY) {
		if (dead) {
			new Exception("Dead cell update").printStackTrace();
		}
		g.setColor(color);
		g.fillOval(x + offX - sizeHalf, y + offY - sizeHalf, size, size);
		if (virus) {
			g.drawImage(virusImg, x + offX - sizeHalf, y + offY - sizeHalf, size, size, null);
		} else {
			if (name.length() > 0) {
				g.setColor(Color.white);
				int size3 = (int) (size * 0.8);
				int sizer = getMaxFittingFontSize(g, defaultFont, name, size3, size3);
				Font fontArial = defaultFont.deriveFont(sizer, sizer);
				g.setFont(fontArial);
				Rectangle2D len = g.getFontMetrics(fontArial).getStringBounds(name, g);
				int lx = (int) ((size - len.getWidth()) / 2);
				int ly = (int) (((size - len.getHeight()) / 2) + len.getHeight());
				g.drawString(name, x + offX - sizeHalf + lx, y + offY - sizeHalf + ly);
			}
		}
	}

	private static int getMaxFittingFontSize(Graphics g, Font font, String string, int width, int height) {
		int minSize = 12;
		int maxSize = 288;
		int curSize = font.getSize();
		while (maxSize - minSize > 2) {
			FontMetrics fm = g.getFontMetrics(new Font(font.getName(), font.getStyle(), curSize));
			int fontWidth = fm.stringWidth(string);
			int fontHeight = fm.getLeading() + fm.getMaxAscent() + fm.getMaxDescent();
			if ((fontWidth > width) || (fontHeight > height)) {
				maxSize = curSize;
				curSize = (maxSize + minSize) / 2;
			} else {
				minSize = curSize;
				curSize = (minSize + maxSize) / 2;
			}
		}
		return curSize;
	}

	public void update(int x2, int y2, int size2, String name2) {
		if (dead) {
			new Exception("Dead cell update").printStackTrace();
		}
		x = x2;
		y = y2;
		size = size2 + size2;
		sizeHalf = size2;
		name = name2.length() > 0 ? name2 : name;
	}

	public void die(mainGUI gui) {
		dead = true;
	}
}
