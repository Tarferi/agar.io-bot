package aga.data;

import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;

public class ImageSource {

	public static Image Virus = ImageLoader(ImageSource.class,"virus.png");

	public static final Image ImageLoader(Class<?> cls, String filename) {
		Image qg = null;
		try {
			qg = ImageIO.read(cls.getResource(filename));
		} catch (Exception e) {
			e.printStackTrace();
			qg = new BufferedImage(20, 20, 1);
		}
		return qg;
	}

}
