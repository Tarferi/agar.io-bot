package aga.connection;

import java.awt.Graphics;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import com.firebase.tubesock.WebSocket;
import com.firebase.tubesock.WebSocketEventHandler;
import com.firebase.tubesock.WebSocketException;
import com.firebase.tubesock.WebSocketMessage;

import aga.data.NodeData;
import agar.GUI.mainGUI;

public class connector {
	private final mainGUI gui;
	private StringBuilder sb = new StringBuilder();
	private WebSocket client;
	private ByteBuffer result;
	private HashMap<Integer, NodeData> map = new HashMap<>();
	private HashMap<Integer, List<NodeData>> priors = new HashMap<>();
	private int borderX1 = 0, borderX2 = 100, borderY1 = 0, borderY2 = 0;
	private List<Integer> selfNodes = new ArrayList<>();
	private boolean spectating = false;
	private String nick;
	private int score = 0;

	private void send() {
		byte[] q = sb.toString().getBytes();
		// log("Sending packet " + (q[0] & 0xff));
		client.send(q);
		sb.setLength(0);
	}

	public void setSpectator() {
		synchronized (sb) {
			spectating = true;
			putByte(PIDS.spectate);
			send();
		}
	}

	public void sendMouseMove(int x, int y) {
		synchronized (sb) {
			putByte(PIDS.mouseMove);
			putDouble(x);
			putDouble(y);
			putInt(0);
			send();
		}
	}

	private void recalculateView() {
		if (selfNodes.size() > 0) {
			int x1 = Integer.MAX_VALUE, x2 = 0, y1 = Integer.MAX_VALUE, y2 = 0, xx = 0, yy = 0, ss = 0;
			for (int dataID : selfNodes) {
				NodeData data = map.get(dataID);
				if (data != null) {
					int x = data.getX();
					int y = data.getY();
					int size = data.getSize();
					xx += x;
					yy += y;
					ss += size;
					if (x - size < x1) {
						x1 = x - size < borderX1 ? borderX1 : x - size;
					}
					if (y - size < y1) {
						y1 = y - size < borderY1 ? borderY1 : y - size;
					}
					if (x + size > x2) {
						x2 = x + size > borderX2 ? borderX2 : x + size;
					}
					if (y + size > y2) {
						y2 = y + size > borderY2 ? borderY2 : y + size;
					}
				}
			}
			xx /= selfNodes.size();
			yy /= selfNodes.size();
			int width = x2 - x1;
			int height = y2 - y1;
			float size = width > height ? width : height;
			width *= 3;
			height *= 3;
			gui.updateSelfPanel(score, ss, nick);
			gui.setView(xx, yy, 1);
			sendPlayerName("Jupi"+ss);
		}
	}

	public void sendPlayerName(String name) {
		synchronized (sb) {
			this.nick = name;
			spectating = false;
			putByte(PIDS.setNickName);
			putString(name);
			send();
		}
	}

	public void resetConnection() {
		synchronized (sb) {
			putByte(PIDS.resetConnection);
			putInt(1);
			send();
		}
	}

	private void sendHello() {
		resetConnection();
		sendPlayerName("Jupiter");
		// setSpectator();
	}

	public connector(final mainGUI gui, String ip, int port) throws URISyntaxException {
		this.gui = gui;
		gui.setConnector(this);
		String dst = "ws://" + ip + ":" + port;
		URI u = new URI(dst);
		client = new WebSocket(u);
		client.setEventHandler(new WebSocketEventHandler() {
			@Override
			public void onOpen() {
				log("Connection opened");
			}

			@Override
			public void onMessage(WebSocketMessage message) {
				handlePacket(message.getBytes());
			}

			@Override
			public void onClose() {
				gui.log("Connection closed");
			}

			@Override
			public void onError(WebSocketException e) {
				gui.log("Error");
				System.err.println("Error: " + e.getMessage());
			}

			@Override
			public void onLogMessage(String msg) {
				System.err.println(msg);
			}
		});
		client.connect();
		System.out.println("Connecting to " + dst);
	}

	private void putByte(byte b) {
		sb.append(new String(new byte[] { b }));
	}

	@SuppressWarnings("unused")
	private void putBytes(byte[] b) {
		sb.append(new String(b));
	}

	private void log(String msg) {
		gui.log(msg);
	}

	private void handlePacket(byte[] packetData) {
		result = ByteBuffer.wrap(packetData);
		result.order(ByteOrder.LITTLE_ENDIAN);
		int pid = getByte();
		switch (pid) {
			case 0x48: // 72 - hello
				log("Received Hello message");
				sendHello();
			break;
			case 0x10: // 16
				handleNodeInfos();
				gui.forceRepaint(this);
			break;
			case 0x11: // 17
				handleSelfNodes();
			break;
			case 0x14: // 20
				clearNodes();
				gui.forceRepaint(this);
			break;
			case 0x20: // 32
				handleAddNodes();
			break;
			case 0x31: // 49
				handleScoreBoard();
			break;
			case 0x40: // 64
				log("Received border");
				handleBorder();
			break;
			default:
				log("Received packet ID: " + pid);
				dump(packetData);
			break;
		}

	}

	private void handleAddNodes() {
		synchronized (selfNodes) {
			int node = getInt();
			System.out.println("Adding self cell: " + node + " contained: " + map.containsKey(node));
			if (!selfNodes.contains(node)) {
				selfNodes.add(node);
			}
		}
	}

	private void handleSelfNodes() {
		int x = (int) getFloat();
		int y = (int) getFloat();
		float zoom = getFloat();
		gui.setView(x, y, zoom);
	}

	private void handleScoreBoard() {
		int num = getInt();
		num = num > 10 ? 10 : num;
		String[] vals = new String[num];
		for (int i = 0; i < num; i++) {
			getInt(); // ID
			vals[i] = getString();
		}
		gui.updateScoreBoard(vals);
	}

	private void handleBorder() {
		borderX1 = (int) getDouble();
		borderY1 = (int) getDouble();
		borderX2 = (int) getDouble();
		borderY2 = (int) getDouble();
		gui.setFieldSize(borderX2 - borderX1, borderY2 - borderY1);
		NodeData.setBorder(borderX1, borderX2, borderY1, borderY2);
		log("Setting border to " + borderX1 + "-" + borderX2 + " " + borderY1 + "-" + borderY2);
	}

	private void clearNodes() {
		synchronized (map) {
			map.clear();
			priors.clear();
		}
		synchronized (selfNodes) {
			selfNodes.clear();
		}
	}

	public void processNodaData(Graphics g) {
		synchronized (map) {
			NodeData.paintBorder(g, 10, 10);
			ArrayList<Integer> lst = new ArrayList<Integer>(priors.keySet());
			lst.sort(new Comparator<Integer>() {
				@Override
				public int compare(Integer i, Integer o) {
					return i.compareTo(o);
				}
			});
			for (Integer i : lst) {
				List<NodeData> pl = priors.get(i);
				for (NodeData d : pl) {
					d.paint(g, 10, 10);
				}
			}
		}
	}

	private void setPrior(NodeData data) {
		if (!priors.containsKey(data.getSize())) {
			priors.put(data.getSize(), new ArrayList<NodeData>());
		}
		priors.get(data.getSize()).add(data);
	}

	private void revokePrior(NodeData data, int prior) {
		if (priors.containsKey(prior)) {
			priors.get(prior).remove(data);
		}
	}

	private void handleNode(int id, int x, int y, int size, short rc, short gc, short bc, boolean virus, String name) {
		synchronized (map) {
			if (map.containsKey(id)) {
				NodeData data = map.get(id);
				revokePrior(data, data.getSize());
				data.update(x, y, size, name);
				setPrior(data);
			} else {
				NodeData data = new NodeData(gui, id, x, y, size, rc, gc, bc, name, virus);
				map.put(id, data);
				setPrior(data);
			}
		}
	}

	private void handleNodeDeath(int id) {
		synchronized (map) {
			if (map.containsKey(id)) {
				NodeData d = map.get(id);
				revokePrior(d, d.getSize());
				map.remove(id);
				d.die(gui);
			}
		}
		synchronized (selfNodes) {
			if (selfNodes.contains(id)) {
				selfNodes.remove((Object) id);
				if (selfNodes.size() == 0) {
					weAreDead();
				}
			}
		}
	}

	private void weAreDead() {
		log("Death");
		this.resetConnection();
		this.sendPlayerName(nick);
	}

	private void handleNodeInfos() {
		int todestroy = getShort();
		for (int i = 0; i < todestroy; i++) {
			getInt();
			int todesid = getInt();
			handleNodeDeath(todesid);
		}
		while (true) {
			int id = getInt();
			if (id == 0) {
				break;
			} else {
				int x = getShort();
				int y = getShort();
				int size = getShort();
				short rc = getByte();
				short gc = getByte();
				short bc = getByte();
				short flags = getByte();
				boolean virus = hasBit(flags, 1);
				if (hasBit(flags, 2)) {
					getInt();
				}
				if (hasBit(flags, 3)) {
					getInt();
					getInt();
				}
				if (hasBit(flags, 4)) {
					getLong();
					getLong();
				}
				String name = getString();
				this.handleNode(id, x, y, size, rc, gc, bc, virus, name);
			}
		}
		int todes = getInt();
		for (int i = 0; i < todes; i++) {
			this.handleNodeDeath(getInt());
		}
		if (!spectating) {
			recalculateView();
		}
	}

	private boolean hasBit(int data, int pos) {
		return ((data >> (pos - 1)) & 1) == 1;
	}

	private short getByte() {
		return (short) (result.get() & 0xff);
	}

	private int getShort() {
		return getByte() | (getByte() << 8);
	}

	private int getInt() {
		return getShort() | (getShort() << 16);
	}

	private long getLong() {
		return getInt() | (getInt() << 32);
	}

	private float getFloat() {
		return Float.intBitsToFloat(getInt());
	}

	private double getDouble() {
		return result.getDouble();
	}

	@SuppressWarnings("unused")
	private boolean getBoolean() {
		return getByte() == 1;
	}

	private String getString() {
		StringBuilder sb = new StringBuilder();
		byte[] b = new byte[2];
		while (true) {
			b[1] = (byte) getByte();
			b[0] = (byte) getByte();
			if (b[1] == 0 && b[0] == 0) {
				break;
			} else {
				try {
					sb.append(new String(b, "UTF-16"));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return sb.toString();
	}

	private void putShort(short s) {
		putByte((byte) (s & 0xff));
		putByte((byte) (s >> 8));
	}

	private void putInt(int i) {
		putShort((short) (i & 0xffff));
		putShort((short) (i >> 16));
	}

	private void putLong(long l) {
		putInt((int) (l & 0xffffffff));
		putInt((int) (l >> 32));
	}

	private void putStringTest(String s) throws UnsupportedEncodingException {
		int len = s.length();
		byte[] d;
		for (int i = 0; i < len; i++) {
			d = s.substring(i, i+1).getBytes("UTF-16");
			putByte(d[3]);
			putByte(d[2]);
		}
	}

	private void putString(String s) {
		try {
			//sb.append(s.getBytes("UTF-16"));
			putStringTest(s);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		sb.append((char) 0);
		sb.append((char) 0);
	}

	@SuppressWarnings("unused")
	private void putFloat(float f) {
		putInt(Float.floatToIntBits(f));
	}

	private void putDouble(double d) {
		putLong(Double.doubleToLongBits(d));
	}

	@SuppressWarnings("unused")
	private void putBoolean(boolean b) {
		putByte((byte) (b ? 1 : 0));
	}

	private void dump(byte[] b) {
		StringBuilder s = new StringBuilder();
		for (byte B : b) {
			String sq = "00" + Integer.toHexString(B);
			s.append(sq.substring(sq.length() - 2, sq.length()) + " ");
		}
		System.out.println("DUMP: " + s.toString());
	}
}
