package agar.GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

public class Scoreboard extends JPanel {
	private static final long serialVersionUID = -3911997129675557207L;
	private JTable table;
	private DefaultTableModel model;
	private static final int max = 10;
	private static final String base = "Player ";

	public Scoreboard() {
		super();
		construct();
	}

	public void reset() {
		model.setRowCount(0);
		model.setDataVector(getDefaultValues(), new Object[] { "#", "Nick" });
	}

	public void setValues(String[] vals) {
		for (int i = 0; i < vals.length; i++) {
			model.setValueAt(vals[i], i, 1);
		}
	}

	private int cx = 0, cy = 0;

	private void construct() {
		Container pane = this;
		super.setSize(125, 170);
		pane.setLayout(new BorderLayout());
		pane.setBackground(Color.black);
		final Scoreboard instance = this;
		pane.setForeground(Color.green);
		table = new JTable();
		table.setBackground(Color.black);
		table.setForeground(Color.green);
		model = new DefaultTableModel(getDefaultValues(), new Object[] { "#", "Nick" });
		table.setModel(model);
		table.getColumnModel().getColumn(0).setPreferredWidth(20);
		pane.add(table, BorderLayout.CENTER);
		setBorder(new LineBorder(Color.yellow, 5));
		table.setEnabled(false);
		table.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				e.consume();
			}

			@Override
			public void mousePressed(MouseEvent e) {
				cx = e.getX();
				cy = e.getY();
			}
		});
		table.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				instance.setLocation(e.getX() + instance.getX() - cx, e.getY() + instance.getY() - cy);
			}
		});
		setVisible(true);
	}

	private Object[][] getDefaultValues() {
		Object[][] o = new Object[max][];
		for (int i = 0; i < max; i++) {
			o[i] = new Object[] { i + 1, base + (i + 1) };
		}
		return o;
	}
}
