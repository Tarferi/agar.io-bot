package agar.GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;

import aga.connection.connector;
import aga.data.NodeData;

@SuppressWarnings("unused")
public class mainGUI extends JFrame {
	private static final long serialVersionUID = -7838487054689633871L;

	public static mainGUI load() {
		return new mainGUI();
	}

	private JPanel loc;
	private ScrollableTextPane logPane;
	private connector con;
	private boolean ctrl = false;
	private JScrollPane scroll;
	private double scale = 1.0;
	private Scoreboard sb;
	private SelfPanel sp;
	private boolean defaultCam;
	protected int scrollWidth;
	protected int scrollHeight;
	private int scrollWidthHalf;
	private int scrollHeightHalf;

	private mainGUI() {

		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				construct();
				setVisible(true);
			}

		});
	}

	public void setConnector(connector con) {
		this.con = con;
	}

	public void refresh() {
		loc.repaint();
	}

	private void construct() {
		setSize(1200, 800);
		setTitle("Agar");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		JPanel cp = new JPanel();
		JPanel op = new JPanel();
		sb = new Scoreboard();
		op.add(sb);
		sb.setLocation(50, 50);
		sp = new SelfPanel(this);
		op.add(sp);
		sp.setLocation(300, 300);
		setContentPane(cp);
		setGlassPane(op);
		op.setOpaque(false);
		op.setVisible(true);

		cp.setLayout(new BorderLayout());
		loc = new JPanel() {
			private static final long serialVersionUID = 6565792951391817737L;

			@Override
			public void paint(Graphics g) {
				super.paint(g);
				((Graphics2D) g).scale(scale, scale);
				con.processNodaData(g);
			}
		};
		scroll = new JScrollPane(loc);
		scroll.getHorizontalScrollBar().setUnitIncrement(16);
		scroll.getVerticalScrollBar().setUnitIncrement(16);
		scroll.addComponentListener(new ComponentAdapter() {

			@Override
			public void componentResized(ComponentEvent arg0) {
				updateScrollSize();
			}
		});
		handScroller scrollListener = new handScroller(scroll.getViewport(), loc);
		loc.addMouseMotionListener(scrollListener);
		loc.addMouseListener(scrollListener);
		loc.setBorder(LineBorder.createBlackLineBorder());
		loc.setBackground(Color.GRAY);
		MouseMotionAdapter l = new MouseMotionAdapter() {

			@Override
			public void mouseMoved(MouseEvent e) {
				con.sendMouseMove(e.getX(), e.getY());
			}

		};
		loc.addMouseMotionListener(l);
		MouseWheelListener wheellistener = new MouseWheelListener() {

			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				if (ctrl && !defaultCam) {
					boolean up = e.getWheelRotation() > 0;
					float zoom = (float) (up ? 0.9 : 1.1);
					scale = scale * zoom;
					e.consume();
				} else {
					scroll.dispatchEvent(e);
				}

			}
		};
		KeyAdapter keylist = new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {
				ctrl = e.getKeyCode() == KeyEvent.VK_CONTROL;
			}

			@Override
			public void keyReleased(KeyEvent e) {
				ctrl = false;
			}
		};
		loc.addKeyListener(keylist);
		scroll.addKeyListener(keylist);
		loc.addMouseWheelListener(wheellistener);
		scroll.addMouseWheelListener(wheellistener);
		logPane = new ScrollableTextPane();
		logPane.getComponent().setPreferredSize(new Dimension(500, 200));
		cp.add(scroll, BorderLayout.CENTER);
		cp.add(logPane.getComponent(), BorderLayout.SOUTH);
		loc.setSize(1000, 1000);
		loc.setPreferredSize(new Dimension(1000, 1000));
		loc.setFocusable(true);
		updateScrollSize();
		loc.requestFocus();
	}

	private void updateScrollSize() {
		scrollWidth = scroll.getWidth();
		scrollHeight = scroll.getHeight();
		scrollWidthHalf = scrollWidth / 2;
		scrollHeightHalf = scrollHeight / 2;
	}

	public void updateScoreBoard(String[] vals) {
		sb.setValues(vals);
	}

	public void log(String msg) {
		if (logPane != null) {
			logPane.append(msg + "\n");
		} else {
			System.out.println("Cons: " + msg);
		}
	}

	public void forceRepaint(connector con) {
		loc.repaint();
	}

	public void setFieldSize(int x, int y) {
		loc.setPreferredSize(new Dimension(x, y));
		loc.setSize(x, y);
	}

	public void forceView(int x, int y, float zoom) {
		scale = zoom;
		scroll.getHorizontalScrollBar().setValue((int) (x * scale) - scrollWidthHalf);
		scroll.getVerticalScrollBar().setValue((int) (y * scale) - scrollHeightHalf);
	}

	public void setView(int x, int y, float zoom) {
		if (defaultCam) {
			forceView(x, y, zoom);
		}
	}
	
	public void updateSelfPanel(int scor, int matte, String nam) {
		sp.update(scor, matte, nam);
	}

	public void setCamState(boolean state) {
		scale = 1;
		this.defaultCam = state;
	}
}
