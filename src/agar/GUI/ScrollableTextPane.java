package agar.GUI;

import java.awt.Color;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

import javax.swing.BoundedRangeModel;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.UIDefaults;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

public class ScrollableTextPane {
	private boolean scroll = false;
	private JScrollPane scrollPane;
	private JTextPane consolepane;

	public JComponent getComponent() {
		return scrollPane;
	}
	
	public synchronized void append(String message) {
		scroll = scrollPane.getVerticalScrollBar().getValue() + scrollPane.getVerticalScrollBar().getModel().getExtent()+50 >= scrollPane.getVerticalScrollBar().getMaximum();
		Document doc = consolepane.getDocument();
		try {
			doc.insertString(doc.getLength(), message, null);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	public ScrollableTextPane() {
		consolepane = new JTextPane();
		consolepane.setBackground(new Color(0, 0, 0));
		consolepane.setText("komnzoleeee");
		scrollPane = new JScrollPane();
		scrollPane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {

			BoundedRangeModel brm = scrollPane.getVerticalScrollBar().getModel();

			public void adjustmentValueChanged(AdjustmentEvent e) {
				if (scroll) {
					brm.setValue(brm.getMaximum());
					scroll = false;
				}
			}
		});
		scrollPane.setViewportView(consolepane);
		UIDefaults defaults = new UIDefaults();
		defaults.put("TextPane[Enabled].backgroundPainter", Color.cyan);
		consolepane.putClientProperty("Nimbus.Overrides", defaults);
		consolepane.putClientProperty("Nimbus.Overrides.InheritDefaults", true);
		consolepane.setBackground(Color.black);
		consolepane.setForeground(Color.cyan);
		consolepane.setEditable(false);
	}
}
