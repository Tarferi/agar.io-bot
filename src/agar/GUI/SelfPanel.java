package agar.GUI;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class SelfPanel extends JPanel {
	private static final long serialVersionUID = -3911997129675557207L;

	public SelfPanel(mainGUI g) {
		super();
		construct(g);
	}

	private int cx = 0, cy = 0;
	private JCheckBox cam;
	private JLabel score;
	private JLabel matter;
	private JLabel name;
	private MouseAdapter ma;
	private MouseMotionAdapter mma;

	public void update(int scor, int matte, String nam) {
		score.setText("Score: " + scor);
		matter.setText("Matter: " + matte);
		name.setText("Name: " + nam);
	}

	private void setDraggable(JComponent c) {
		c.addMouseListener(ma);
		c.addMouseMotionListener(mma);
	}

	private void construct(final mainGUI g) {
		final SelfPanel instance = this;
		mma = new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				instance.setLocation(e.getX() + instance.getX() - cx, e.getY() + instance.getY() - cy);
			}
		};
		ma = new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				e.consume();
			}

			@Override
			public void mousePressed(MouseEvent e) {
				cx = e.getX();
				cy = e.getY();
			}
		};
		Container pane = this;
		super.setSize(300, 500);
		pane.setLayout(new BoxLayout(pane,BoxLayout.Y_AXIS));
		cam = new JCheckBox("Default camera", true);
		cam.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				g.setCamState(cam.isSelected());
			}
			
		});
		g.setCamState(true);
		score = new JLabel("Score: 0");
		matter = new JLabel("Matter: 0");
		name = new JLabel("Name: ");
		pane.add(name);
		pane.add(score);
		pane.add(matter);
		pane.add(cam);
		pane.setForeground(Color.green);
		setDraggable((JComponent) pane);
		setDraggable((JComponent) cam);
		setDraggable((JComponent) score);
		setDraggable((JComponent) matter);
		setDraggable((JComponent) name);
		setVisible(true);
	}
}
