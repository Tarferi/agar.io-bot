package agar.GUI;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;
import javax.swing.JViewport;

public class handScroller extends MouseAdapter {
	private final Point pp = new Point();
	private JPanel panel;
	private JViewport vport;

	public handScroller(JViewport vport, JPanel image) {
		this.vport=vport;
		this.panel = image;
	}
	
	@Override
	public void mouseDragged(final MouseEvent e) {
		Point cp = e.getPoint();
		Point vp = vport.getViewPosition();
		vp.translate(pp.x - cp.x, pp.y - cp.y);
		panel.scrollRectToVisible(new Rectangle(vp, vport.getSize()));
		pp.setLocation(cp);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		pp.setLocation(e.getPoint());
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		panel.repaint();
	}
}